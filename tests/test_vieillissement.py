import numpy as np
import os
import pytest
import unittest

from openfisca_survey_manager.paths import default_config_files_directory

from leximpact_survey_scenario import root_path
from leximpact_survey_scenario.leximpact_survey_scenario import (
    LeximpactErfsSurveyScenario,
    leximpact_tbs,
)
from leximpact_survey_scenario.scenario_tools.inflation_calibration_values import (
    inflation_coefs,
)

try:
    assert os.path.exists(os.path.join(default_config_files_directory, "config.ini"))
    config_files_directory = default_config_files_directory

except AssertionError:
    config_files_directory = os.path.join(
        root_path, ".config", "openfisca-survey-manager"
    )

tc = unittest.TestCase()


@pytest.mark.asyncio
async def test_viellissement(period=2025):
    leximpact_survey_scenario = LeximpactErfsSurveyScenario(
        config_files_directory=config_files_directory,
        period=period,
        amendement_tax_benefit_system=leximpact_tbs,
        baseline_tax_benefit_system=leximpact_tbs,
    )

    input_variables = leximpact_survey_scenario.used_as_input_variables
    input_variables = [
        i
        for i in input_variables
        if i
        not in [
            "weight_familles",
            "weight_foyers",
            "weight_individus",
            "weight_menages",
        ]
    ]

    for year in [period, period - 1]:
        inflation_rate = inflation_coefs(
            variables=input_variables + ["weight_individus"],
            startp=str(year - 1),
            endp=str(year),
            ajustement_pop=False,
        )

        for var in input_variables:
            if leximpact_survey_scenario.tax_benefit_systems["baseline"].get_variable(
                var
            ).value_type in [float, int, bool]:
                agg_period = leximpact_survey_scenario.simulations[
                    "baseline"
                ].compute_aggregate(
                    var,
                    period=year,
                )
                agg_period_n_1 = leximpact_survey_scenario.simulations[
                    "baseline"
                ].compute_aggregate(
                    var,
                    period=(year - 1),
                )
                if var in inflation_rate.keys():
                    if (
                        var == "loyer"
                    ):  # cas special du loyer car l'augmentation se fait sans corriger de la variation de pop donc le resultat est (inflateur loyer) * (inflateur pop)
                        np.testing.assert_almost_equal(
                            agg_period / agg_period_n_1,
                            inflation_rate[var],
                            decimal=2,
                            err_msg=f'Pour la variable {var}, cas 1, ratio attendu {inflation_rate[var] * inflation_rate["weight_individus"]}, ratio optenu {agg_period / agg_period_n_1}',
                            verbose=True,
                        )
                    else:
                        np.testing.assert_almost_equal(
                            agg_period / agg_period_n_1,
                            inflation_rate[var],
                            decimal=2,
                            err_msg=f"Pour la variable {var}, cas 1, ratio attendu {inflation_rate[var]}, ratio optenu {agg_period / agg_period_n_1}",
                            verbose=True,
                        )
                else:
                    np.testing.assert_almost_equal(
                        agg_period / agg_period_n_1,
                        inflation_rate["weight_individus"],
                        decimal=2,
                        err_msg=f'Pour la variable {var}, cas 2, ratio attendu {inflation_rate["weight_individus"]}, ratio optenu {agg_period / agg_period_n_1}',
                        verbose=True,
                    )
