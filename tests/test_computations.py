import numpy as np
import os
import pytest
import unittest


from openfisca_survey_manager.paths import default_config_files_directory
from openfisca_survey_manager.simulations import SecretViolationError


from leximpact_survey_scenario import root_path
from leximpact_survey_scenario.leximpact_survey_scenario import (
    LeximpactErfsSurveyScenario,
    leximpact_tbs,
)

try:
    assert os.path.exists(os.path.join(default_config_files_directory, "config.ini"))
    config_files_directory = default_config_files_directory

except AssertionError:
    config_files_directory = os.path.join(
        root_path, ".config", "openfisca-survey-manager"
    )

print(f"dirpath : {config_files_directory}")

period = 2025

leximpact_survey_scenario = LeximpactErfsSurveyScenario(
    config_files_directory=config_files_directory,
    period=period,
    amendement_tax_benefit_system=leximpact_tbs,
    baseline_tax_benefit_system=leximpact_tbs,
    plf_tax_benefit_system=leximpact_tbs,
)

tc = unittest.TestCase()


@pytest.mark.asyncio
async def test_compute_aggregate():
    niveau_de_vie_baseline = await leximpact_survey_scenario.compute_aggregate(
        "niveau_de_vie",
        simulation="baseline",
        period=period,
    )
    niveau_de_vie_rel_diff = (
        await leximpact_survey_scenario.compute_aggregate(
            "niveau_de_vie",
            simulation="plf",
            baseline_simulation="baseline",
            period=period,
        )
        / niveau_de_vie_baseline
    )
    np.testing.assert_almost_equal(niveau_de_vie_rel_diff, 0, decimal=2)

    # niveau_de_vie = await leximpact_survey_scenario.compute_aggregate(
    #     "niveau_de_vie",
    #     simulation="plf",
    #     period=period,
    # )
    # tc.assertAlmostEqual(niveau_de_vie, 788_715_503_437, delta=1e9)
    # np.testing.assert_almost_equal(775_100_413_555 / niveau_de_vie, 1, decimal=2)


@pytest.mark.asyncio
async def test_create_data_frame_by_entity():
    leximpact_survey_scenario.create_data_frame_by_entity(
        variables=["niveau_de_vie"],
        simulation="baseline",
        period=period,
    )


@pytest.mark.asyncio
async def test_summarize_by_quantile():
    summary = leximpact_survey_scenario.summarize_by_quantile(
        variables=["impot_revenu_restant_a_payer", "rfr"],
        by="decile_rfr",
        simulation="baseline",
        baseline_simulation="baseline",
        period=period,
        format="dict",
    )
    with pytest.raises(SecretViolationError):
        summary = leximpact_survey_scenario.summarize_by_quantile(
            variables=["impot_revenu_restant_a_payer", "rfr"],
            by="decile_rfr",
            simulation="baseline",
            baseline_simulation="baseline",
            period=period,
            format="dict",
            share_threshold=1e-6,
        )
    summary = leximpact_survey_scenario.summarize_by_quantile(
        variables=["impot_revenu_restant_a_payer", "rfr"],
        by="decile_rfr_par_part",
        simulation="baseline",
        baseline_simulation="baseline",
        period=period,
        format="dict",
    )
    print(summary)


@pytest.mark.asyncio
async def test_compute_winners_loosers():
    winners_loosers = leximpact_survey_scenario.compute_winners_loosers(
        variable="impot_revenu_restant_a_payer",
        simulation="plf",
        baseline_simulation="baseline",
        period=period,
        # absolute_minimal_detected_variation = 0,
        # relative_minimal_detected_variation = .01,
        # observations_threshold = None,
    )
    print(winners_loosers)


@pytest.mark.asyncio
async def test_calibration():
    leximpact_survey_scenario.create_data_frame_by_entity(
        variables=["rfr"],
        simulation="baseline",
        period=period,
    )
    rfr_before = leximpact_survey_scenario.simulations["baseline"].compute_aggregate(
        "rfr", period=period
    )
    rfr_target = 130_000_000_000
    assert rfr_before / rfr_target != 1
    leximpact_survey_scenario.calibrate(
        target_margins_by_variable={"rfr": rfr_target},
        parameters={"method": "raking ratio"},
        period=period,
    )

    rfr_after = leximpact_survey_scenario.simulations["baseline"].compute_aggregate(
        "rfr", period=period
    )
    np.testing.assert_almost_equal(rfr_after / rfr_target, 1, decimal=2)
