from openfisca_france_data.aggregates import FranceAggregates as Aggregates  # type: ignore
from leximpact_survey_scenario.leximpact_survey_scenario import (  # type: ignore
    LeximpactErfsSurveyScenario,
)

import click
import os
import pandas as pd
from numpy import where
from leximpact_survey_scenario import root_path

aggregate_variables = [
    "cotisations_salariales",
    "cotisations_employeur",
    "cotisations_non_salarie",
    "salaire_de_base",
    "traitement_indicidiaire_brut",
    "retraite_brute",
    "chomage_brut",
    "revenu_categoriel_foncier",  # "revenu_categoriel_plus_values", #"revenu_categoriel_capital",
    "salaire_imposable",
    "retraite_imposable",
    "chomage_imposable",
    "csg_salaire",
    "csg_non_salarie",
    "csg_remplacement",
    "csg_revenus_capital",
    "csg",
    "crds",
    "impot_revenu_restant_a_payer",
    "irpp_economique",
    "prelevement_forfaitaire_unique_ir",
    "prelevement_forfaitaire_unique_ir_plus_values",
    "prelevement_forfaitaire_non_liberatoire",
    "prelevement_forfaitaire_liberatoire",
    "af",
    "af_base",
    "cf",
    "paje",
    "ars",
    "asf",
    "aspa",
    "rsa",
    "ppa",  #'aah', #'caah',
    "aides_logement",
]


def test_erfs_survey_simulation(
    period: int = 2025,
    update_targets=False,
    root_path=root_path,
    aggregate_variables=aggregate_variables,
):
    config_dirpath = os.path.join(root_path, ".config", "openfisca-survey-manager")
    # On ititialise le survey scenario
    survey_scenario = LeximpactErfsSurveyScenario(
        period=period, config_files_directory=config_dirpath
    )
    # On calcule les agrégats
    aggregates = Aggregates(
        survey_scenario=survey_scenario, target_source="france_entiere"
    )
    aggregates.aggregate_variables = aggregate_variables
    aggregats = aggregates.compute_aggregates(use_baseline=True, actual=False)
    aggregats["variable"] = aggregats.index
    aggregats.reset_index(inplace=True)
    aggregats = aggregats[["variable", "baseline_amount", "baseline_beneficiaries"]]

    path = os.path.join(
        root_path, "tests", "aggregats_targets", f"aggregats_{period}.py"
    )

    if not os.path.exists(path):
        print(
            f"Il n'y a pas d'agrégats de référence pour l'année {period}, cette simulation va servir de cibles"
        )
        update_targets = True

    if update_targets:
        aggregats.columns = [
            "variable",
            "baseline_amount_target",
            "baseline_beneficiaries_target",
        ]
        aggregats = aggregats.set_index("variable").to_dict(orient="index")
        with open(
            f"{root_path}/tests/aggregats_targets/aggregats_{period}.py",
            "w",
        ) as f:
            f.write(f"{aggregats}")

    else:
        with open(
            f"{root_path}/tests/aggregats_targets/aggregats_{period}.py",
            "r",
        ) as f:
            targets = pd.DataFrame.from_dict(eval(f.read()), orient="index")
            targets = targets.rename_axis("variable").reset_index()

        diff = pd.merge(targets, aggregats, how="inner", on="variable")
        diff["comp_amount"] = round(
            (diff.baseline_amount / diff.baseline_amount_target) - 1, ndigits=2
        )
        diff["comp_beneficiaries"] = round(
            (diff.baseline_beneficiaries / diff.baseline_beneficiaries_target) - 1,
            ndigits=2,
        )
        diff["comp_amount"] = where(
            (diff.baseline_amount_target == 0) & (diff.baseline_amount != 0),
            1,
            diff.comp_amount,
        )
        diff["comp_amount"] = where(
            (diff.baseline_amount_target == 0) & (diff.baseline_amount == 0),
            0,
            diff.comp_amount,
        )
        diff["comp_beneficiaries"] = where(
            (diff.baseline_beneficiaries_target == 0)
            & (diff.baseline_beneficiaries != 0),
            1,
            diff.comp_beneficiaries,
        )
        diff["comp_beneficiaries"] = where(
            (diff.baseline_beneficiaries_target == 0)
            & (diff.baseline_beneficiaries == 0),
            0,
            diff.comp_beneficiaries,
        )
        amount_not_equal = diff.loc[diff.comp_amount != 0]
        beneficiaries_not_equal = diff.loc[diff.comp_beneficiaries != 0]

        assert (
            len(amount_not_equal) == 0
        ), f"Les agrégats (sommes) ont changé pour l'année {period} : {amount_not_equal}"
        assert (
            len(beneficiaries_not_equal) == 0
        ), f"Les agrégats (nombre) ont changé pour l'année {period}: {beneficiaries_not_equal}"


# def test_erfs_survey_simulation_2024():
#     test_erfs_survey_simulation(period=2024, update_targets=False)


@click.command()
@click.option(
    "-p",
    "--period",
    "period",
    default=2025,
    help="Year of simulation",
    show_default=True,
    type=int,
    required=False,
)
@click.option(
    "-u",
    "--update_targets",
    "update_targets",
    default=False,
    help="Update targets aggregates",
    show_default=True,
    type=bool,
    required=False,
)
@click.option(
    "-r",
    "--root_path",
    "root_path",
    default=root_path,
    help="Project path",
    show_default=True,
    type=str,
    required=False,
)
def main(period, update_targets, root_path):
    test_erfs_survey_simulation(
        period,
        update_targets,
        aggregate_variables=aggregate_variables,
        root_path=root_path,
    )


if __name__ == "__main__":
    main()
