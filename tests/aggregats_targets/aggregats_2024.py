{
    "cotisations_salariales": {
        "baseline_amount_target": -101670,
        "baseline_beneficiaries_target": 28971,
    },
    "cotisations_employeur": {
        "baseline_amount_target": -385051,
        "baseline_beneficiaries_target": 28971,
    },
    "cotisations_non_salarie": {
        "baseline_amount_target": -18271,
        "baseline_beneficiaries_target": 2606,
    },
    "salaire_de_base": {
        "baseline_amount_target": 779909,
        "baseline_beneficiaries_target": 24961,
    },
    "traitement_indicidiaire_brut": {
        "baseline_amount_target": 0,
        "baseline_beneficiaries_target": 0,
    },
    "retraite_brute": {
        "baseline_amount_target": 319872,
        "baseline_beneficiaries_target": 15626,
    },
    "chomage_brut": {
        "baseline_amount_target": 33971,
        "baseline_beneficiaries_target": 5213,
    },
    "revenu_categoriel_foncier": {
        "baseline_amount_target": 31414,
        "baseline_beneficiaries_target": 4745,
    },
    "salaire_imposable": {
        "baseline_amount_target": 770445,
        "baseline_beneficiaries_target": 28971,
    },
    "retraite_imposable": {
        "baseline_amount_target": 318222,
        "baseline_beneficiaries_target": 15626,
    },
    "chomage_imposable": {
        "baseline_amount_target": 33705,
        "baseline_beneficiaries_target": 5213,
    },
    "csg_salaire": {
        "baseline_amount_target": -84504,
        "baseline_beneficiaries_target": 28971,
    },
    "csg_non_salarie": {
        "baseline_amount_target": -7320,
        "baseline_beneficiaries_target": 2484,
    },
    "csg_remplacement": {
        "baseline_amount_target": 0,
        "baseline_beneficiaries_target": 0,
    },
    "csg_revenus_capital": {
        "baseline_amount_target": -11692,
        "baseline_beneficiaries_target": 14347,
    },
    "csg": {"baseline_amount_target": -124672, "baseline_beneficiaries_target": 42895},
    "crds": {"baseline_amount_target": -7397, "baseline_beneficiaries_target": 45146},
    "impot_revenu_restant_a_payer": {
        "baseline_amount_target": -87297,
        "baseline_beneficiaries_target": 22393,
    },
    "irpp_economique": {
        "baseline_amount_target": -93372,
        "baseline_beneficiaries_target": 22875,
    },
    "prelevement_forfaitaire_unique_ir": {
        "baseline_amount_target": -9879,
        "baseline_beneficiaries_target": 14857,
    },
    "prelevement_forfaitaire_unique_ir_plus_values": {
        "baseline_amount_target": -3804,
        "baseline_beneficiaries_target": 181,
    },
    "prelevement_forfaitaire_non_liberatoire": {
        "baseline_amount_target": 6074,
        "baseline_beneficiaries_target": 14857,
    },
    "prelevement_forfaitaire_liberatoire": {
        "baseline_amount_target": 0,
        "baseline_beneficiaries_target": 0,
    },
    "af": {"baseline_amount_target": 13293, "baseline_beneficiaries_target": 5103},
    "af_base": {"baseline_amount_target": 11926, "baseline_beneficiaries_target": 5100},
    "cf": {"baseline_amount_target": 2106, "baseline_beneficiaries_target": 826},
    "paje": {"baseline_amount_target": 3617, "baseline_beneficiaries_target": 1724},
    "ars": {"baseline_amount_target": 1943, "baseline_beneficiaries_target": 2628},
    "asf": {"baseline_amount_target": 6903, "baseline_beneficiaries_target": 2005},
    "aspa": {"baseline_amount_target": 4735, "baseline_beneficiaries_target": 1144},
    "rsa": {"baseline_amount_target": 11529, "baseline_beneficiaries_target": 2175},
    "ppa": {"baseline_amount_target": 12317, "baseline_beneficiaries_target": 6049},
    "aides_logement": {
        "baseline_amount_target": 13662,
        "baseline_beneficiaries_target": 5495,
    },
}
