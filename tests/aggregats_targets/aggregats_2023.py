{
    "cotisations_salariales": {
        "baseline_amount_target": -101813,
        "baseline_beneficiaries_target": 28886,
    },
    "cotisations_employeur": {
        "baseline_amount_target": -428625,
        "baseline_beneficiaries_target": 28886,
    },
    "cotisations_non_salarie": {
        "baseline_amount_target": -16145,
        "baseline_beneficiaries_target": 2599,
    },
    "salaire_de_base": {
        "baseline_amount_target": 780293,
        "baseline_beneficiaries_target": 24888,
    },
    "traitement_indicidiaire_brut": {
        "baseline_amount_target": 0,
        "baseline_beneficiaries_target": 0,
    },
    "retraite_brute": {
        "baseline_amount_target": 317333,
        "baseline_beneficiaries_target": 15580,
    },
    "chomage_brut": {
        "baseline_amount_target": 33338,
        "baseline_beneficiaries_target": 5197,
    },
    "revenu_categoriel_foncier": {
        "baseline_amount_target": 29155,
        "baseline_beneficiaries_target": 4752,
    },
    "salaire_imposable": {
        "baseline_amount_target": 770682,
        "baseline_beneficiaries_target": 28886,
    },
    "retraite_imposable": {
        "baseline_amount_target": 300937,
        "baseline_beneficiaries_target": 15580,
    },
    "chomage_imposable": {
        "baseline_amount_target": 32120,
        "baseline_beneficiaries_target": 5197,
    },
    "csg_salaire": {
        "baseline_amount_target": -84511,
        "baseline_beneficiaries_target": 28886,
    },
    "csg_non_salarie": {
        "baseline_amount_target": -6621,
        "baseline_beneficiaries_target": 2599,
    },
    "csg_remplacement": {
        "baseline_amount_target": -21047,
        "baseline_beneficiaries_target": 13516,
    },
    "csg_revenus_capital": {
        "baseline_amount_target": -10774,
        "baseline_beneficiaries_target": 14301,
    },
    "csg": {"baseline_amount_target": -122954, "baseline_beneficiaries_target": 42558},
    "crds": {"baseline_amount_target": -7262, "baseline_beneficiaries_target": 44813},
    "impot_revenu_restant_a_payer": {
        "baseline_amount_target": -86620,
        "baseline_beneficiaries_target": 19956,
    },
    "irpp_economique": {
        "baseline_amount_target": -92210,
        "baseline_beneficiaries_target": 22006,
    },
    "prelevement_forfaitaire_unique_ir": {
        "baseline_amount_target": -9104,
        "baseline_beneficiaries_target": 14805,
    },
    "prelevement_forfaitaire_unique_ir_plus_values": {
        "baseline_amount_target": -3514,
        "baseline_beneficiaries_target": 181,
    },
    "prelevement_forfaitaire_non_liberatoire": {
        "baseline_amount_target": 5590,
        "baseline_beneficiaries_target": 14805,
    },
    "prelevement_forfaitaire_liberatoire": {
        "baseline_amount_target": 0,
        "baseline_beneficiaries_target": 0,
    },
    "af": {"baseline_amount_target": 12859, "baseline_beneficiaries_target": 5088},
    "af_base": {"baseline_amount_target": 11534, "baseline_beneficiaries_target": 5086},
    "cf": {"baseline_amount_target": 2033, "baseline_beneficiaries_target": 829},
    "paje": {"baseline_amount_target": 3442, "baseline_beneficiaries_target": 1710},
    "ars": {"baseline_amount_target": 1854, "baseline_beneficiaries_target": 2626},
    "asf": {"baseline_amount_target": 6643, "baseline_beneficiaries_target": 2005},
    "aspa": {"baseline_amount_target": 4093, "baseline_beneficiaries_target": 1103},
    "rsa": {"baseline_amount_target": 10932, "baseline_beneficiaries_target": 2156},
    "ppa": {"baseline_amount_target": 11279, "baseline_beneficiaries_target": 5828},
    "aides_logement": {
        "baseline_amount_target": 13043,
        "baseline_beneficiaries_target": 5451,
    },
}
