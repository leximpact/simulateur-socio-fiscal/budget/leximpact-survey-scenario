{
    "cotisations_salariales": {
        "baseline_amount_target": -107041,
        "baseline_beneficiaries_target": 31141,
    },
    "cotisations_employeur": {
        "baseline_amount_target": -410094,
        "baseline_beneficiaries_target": 31141,
    },
    "cotisations_non_salarie": {
        "baseline_amount_target": -20169,
        "baseline_beneficiaries_target": 2559,
    },
    "salaire_de_base": {
        "baseline_amount_target": 828247,
        "baseline_beneficiaries_target": 27239,
    },
    "traitement_indicidiaire_brut": {
        "baseline_amount_target": 0,
        "baseline_beneficiaries_target": 0,
    },
    "retraite_brute": {
        "baseline_amount_target": 348612,
        "baseline_beneficiaries_target": 15839,
    },
    "chomage_brut": {
        "baseline_amount_target": 36873,
        "baseline_beneficiaries_target": 5223,
    },
    "revenu_categoriel_foncier": {
        "baseline_amount_target": 42008,
        "baseline_beneficiaries_target": 4561,
    },
    "salaire_imposable": {
        "baseline_amount_target": 810015,
        "baseline_beneficiaries_target": 31141,
    },
    "retraite_imposable": {
        "baseline_amount_target": 346999,
        "baseline_beneficiaries_target": 15839,
    },
    "chomage_imposable": {
        "baseline_amount_target": 36592,
        "baseline_beneficiaries_target": 5223,
    },
    "csg_salaire": {
        "baseline_amount_target": -88855,
        "baseline_beneficiaries_target": 31141,
    },
    "csg_non_salarie": {
        "baseline_amount_target": -8405,
        "baseline_beneficiaries_target": 2410,
    },
    "csg_remplacement": {
        "baseline_amount_target": 0,
        "baseline_beneficiaries_target": 0,
    },
    "csg_revenus_capital": {
        "baseline_amount_target": -10116,
        "baseline_beneficiaries_target": 13993,
    },
    "csg": {"baseline_amount_target": -131277, "baseline_beneficiaries_target": 44616},
    "crds": {"baseline_amount_target": -7734, "baseline_beneficiaries_target": 45906},
    "impot_revenu_restant_a_payer": {
        "baseline_amount_target": -95884,
        "baseline_beneficiaries_target": 22799,
    },
    "irpp_economique": {
        "baseline_amount_target": -95884,
        "baseline_beneficiaries_target": 22799,
    },
    "prelevement_forfaitaire_unique_ir": {
        "baseline_amount_target": 7965,
        "baseline_beneficiaries_target": 13585,
    },
    "prelevement_forfaitaire_unique_ir_plus_values": {
        "baseline_amount_target": 0,
        "baseline_beneficiaries_target": 0,
    },
    "prelevement_forfaitaire_non_liberatoire": {
        "baseline_amount_target": 0,
        "baseline_beneficiaries_target": 0,
    },
    "prelevement_forfaitaire_liberatoire": {
        "baseline_amount_target": 0,
        "baseline_beneficiaries_target": 0,
    },
    "af": {"baseline_amount_target": 13102, "baseline_beneficiaries_target": 4865},
    "af_base": {"baseline_amount_target": 11702, "baseline_beneficiaries_target": 4861},
    "cf": {"baseline_amount_target": 2113, "baseline_beneficiaries_target": 809},
    "paje": {"baseline_amount_target": 3405, "baseline_beneficiaries_target": 1618},
    "ars": {"baseline_amount_target": 1883, "baseline_beneficiaries_target": 2515},
    "asf": {"baseline_amount_target": 6074, "baseline_beneficiaries_target": 1752},
    "aspa": {"baseline_amount_target": 5421, "baseline_beneficiaries_target": 1180},
    "rsa": {"baseline_amount_target": 13093, "baseline_beneficiaries_target": 2273},
    "ppa": {"baseline_amount_target": 15574, "baseline_beneficiaries_target": 6644},
    "aides_logement": {
        "baseline_amount_target": 1130,
        "baseline_beneficiaries_target": 2283,
    },
}
