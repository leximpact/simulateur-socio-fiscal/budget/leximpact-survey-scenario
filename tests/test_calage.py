import os
import pytest

from leximpact_survey_scenario import root_path
from leximpact_survey_scenario.leximpact_survey_scenario import (
    LeximpactErfsSurveyScenario,
    leximpact_tbs,
)
from leximpact_survey_scenario.scenario_tools.calage_tax_benefits_system import (
    create_tax_and_benefits_system_with_targets,
)
from openfisca_survey_manager.paths import default_config_files_directory

try:
    assert os.path.exists(os.path.join(default_config_files_directory, "config.ini"))
    config_files_directory = default_config_files_directory

except AssertionError:
    config_files_directory = os.path.join(
        root_path, ".config", "openfisca-survey-manager"
    )
period = 2025


@pytest.mark.asyncio
async def test_tax_benefit_system_cale():
    leximpact_survey_scenario = LeximpactErfsSurveyScenario(
        config_files_directory=config_files_directory,
        period=period,
        baseline_tax_benefit_system=leximpact_tbs,
    )

    irpp_economique_avant_calage = await leximpact_survey_scenario.compute_aggregate(
        "irpp_economique",
        simulation="baseline",
        period=period,
    )
    csg_retraite_avant_calage = await leximpact_survey_scenario.compute_aggregate(
        "csg_retraite",
        simulation="baseline",
        period=period,
    )

    leximpact_tbs_cale = create_tax_and_benefits_system_with_targets(
        {"irpp_economique": 2},
        variables_a_caler=["irpp_economique"],
        tax_and_benefits_system=leximpact_tbs,
    )
    leximpact_survey_scenario_cale = LeximpactErfsSurveyScenario(
        config_files_directory=config_files_directory,
        period=period,
        baseline_tax_benefit_system=leximpact_tbs_cale,
    )

    irpp_economique_apres_calage = (
        await leximpact_survey_scenario_cale.compute_aggregate(
            "irpp_economique",
            simulation="baseline",
            period=period,
        )
    )
    csg_retraite_apres_calage = await leximpact_survey_scenario_cale.compute_aggregate(
        "csg_retraite",
        simulation="baseline",
        period=period,
    )

    assert irpp_economique_apres_calage == 2 * irpp_economique_avant_calage
    assert csg_retraite_avant_calage == csg_retraite_apres_calage
