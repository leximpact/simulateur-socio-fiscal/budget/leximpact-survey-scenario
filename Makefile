.ONESHELL:
SHELL := /bin/bash

install:
	poetry install -E france-reforms
	poetry run pre-commit install

precommit:
	poetry run ruff --fix leximpact_survey_scenario/* tests/*
	poetry run pre-commit run --all-files

test:
	poetry run pytest


debug_test:
	poetry run pytest --pdb

bump_patch:
	poetry version patch
	$(MAKE) precommit

bump_minor:
	poetry version minor
	$(MAKE) precommit

bump_major:
	poetry version major
	$(MAKE) precommit
