import logging
import numpy as np
import openfisca_france
from numpy import datetime64
from openfisca_core import reforms
from openfisca_core.errors import VariableNameConflictError
from openfisca_core.holders.helpers import set_input_dispatch_by_period
from openfisca_france.model.revenus.activite.salarie import (
    TypesAllegementModeRecouvrement,
)

try:
    from openfisca_survey_manager.statshelpers import mark_weighted_percentiles
except ImportError:
    mark_weighted_percentiles = None

from openfisca_france_data.model.base import (
    ADD,
    ETERNITY,
    YEAR,
    MONTH,
    Individu,
    Menage,
    FoyerFiscal,
    Famille,
    Variable,
    Enum,
    Deciles,
    calculate_output_add,
    set_input_divide_by_period,
    min_,
    max_,
    not_,
)

log = logging.getLogger(__name__)

neutralized_variables = [
    # Neutralisation de variables composantes du traitement indicidaire car elles ne sont pas identifiables dans les données ERFS-FPR
    "indemnite_residence",
    "supplement_familial_traitement",
    "indemnite_compensatrice_csg",
    # TH
    "taxe_habitation",
]

foyer_fiscal_projected_variables = [
    "csg_deductible_salaire",
    "csg_imposable_salaire",
    "csg_deductible_retraite",
    "csg_imposable_retraite",
    "csg_salaire",
    "csg_retraite",
]

projected_variables = {
    "menage": {"famille": ["af"]},
    "foyer_fiscal": {"individu": foyer_fiscal_projected_variables},
}

# projected_variable = {'final_entity' : {'openfisca_france_entity' : [variables]}}

openfisca_france_tax_benefit_system = openfisca_france.FranceTaxBenefitSystem()


def create_leximpact_tbs(projected_variables=projected_variables):
    class leximpact_tbs_extension(reforms.Reform):
        def apply(self):
            class quimen(Variable):
                is_period_size_independent = True
                value_type = int
                entity = Individu
                label = "Rôle dans le ménage"
                definition_period = ETERNITY

            class quifam(Variable):
                is_period_size_independent = True
                value_type = int
                entity = Individu
                label = "Rôle dans la famille"
                definition_period = ETERNITY

            class quifoy(Variable):
                is_period_size_independent = True
                value_type = int
                entity = Individu
                label = "Rôle dans le foyer fiscal"
                definition_period = ETERNITY

            class idmen(Variable):
                is_period_size_independent = True
                value_type = int
                entity = Individu
                label = "Identifiant ménage dans openfisca-france-data"
                definition_period = ETERNITY

            class idmen_original(Variable):
                is_period_size_independent = True
                value_type = int
                entity = Menage
                label = "Identifiant ménage dans erfs-fpr"
                definition_period = ETERNITY

            class idfam(Variable):
                is_period_size_independent = True
                value_type = int
                entity = Individu
                label = "Identifiant famille dans openfisca-france-data"
                definition_period = ETERNITY

            class idfoy(Variable):
                is_period_size_independent = True
                value_type = int
                entity = Individu
                label = "Identifiant foyer fiscal dans openfisca-france-data"
                definition_period = ETERNITY

            class menage_id(Variable):
                is_period_size_independent = True
                value_type = int
                entity = Individu
                label = "Identifiant ménage"  # dans openfisca-survey-manager ?
                definition_period = ETERNITY

            class famille_id(Variable):
                is_period_size_independent = True
                value_type = int
                entity = Individu
                label = "Identifiant famille"  # dans openfisca-survey-manager ?
                definition_period = ETERNITY

            class foyer_fiscal_id(Variable):
                is_period_size_independent = True
                value_type = int
                entity = Individu
                label = "Identifiant foyer fiscal"  # dans openfisca-survey-manager ?
                definition_period = ETERNITY

            class weight_familles(Variable):
                is_period_size_independent = True
                value_type = float
                entity = Famille
                label = "Poids de la famille"
                definition_period = YEAR

                def formula(famille, period):
                    return famille.demandeur("weight_individus", period)

            class weight_foyers(Variable):
                is_period_size_independent = True
                value_type = float
                entity = FoyerFiscal
                label = "Poids du foyer fiscal"
                definition_period = YEAR

                def formula(foyer_fiscal, period):
                    return foyer_fiscal.declarant_principal("weight_individus", period)

            class weight_menages(Variable):
                is_period_size_independent = True
                value_type = float
                entity = Menage
                label = "Poids du ménage"
                definition_period = YEAR

                def formula(menage, period):
                    return menage.personne_de_reference("weight_individus", period)

            class wprm(Variable):
                default_value = 1.0
                is_period_size_independent = True
                value_type = float
                entity = Menage
                label = "Effectifs"
                definition_period = YEAR

            class weight_individus(Variable):
                is_period_size_independent = True
                value_type = float
                entity = Individu
                label = "Poids de l'individu"
                definition_period = YEAR

                def formula(individu, period):
                    return individu.menage("wprm", period)

            class rpns_imposables(Variable):
                value_type = float
                entity = Individu
                label = "Revenus imposables des professions non salariées individuels"
                definition_period = YEAR

                def formula(individu, period):
                    rag = individu("rag", period)
                    ric = individu("ric", period)
                    rnc = individu("rnc", period)

                    return rag + ric + rnc

            class rfr_plus_values_hors_rni(Variable):
                value_type = float
                entity = FoyerFiscal
                label = "Plus-values hors RNI entrant dans le calcul du revenu fiscal de référence (PV au barème, PV éxonérées ..)"
                definition_period = YEAR

                def formula_2018_01_01(foyer_fiscal, period):
                    return foyer_fiscal(
                        "plus_values_prelevement_forfaitaire_unique_ir", period
                    )
                    # return foyer_fiscal("assiette_csg_plus_values", period)

            # class plus_values_prelevement_forfaitaire_unique_ir(Variable):
            #     value_type = float
            #     entity = FoyerFiscal
            #     label = "Plus-values soumises au prélèvement forfaitaire unique (partie impôt sur le revenu)"
            #     reference = "https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000036377422/"
            #     definition_period = YEAR

            #     def formula_2018_01_01(foyer_fiscal, period):
            #         return foyer_fiscal("assiette_csg_plus_values", period)

            class assiette_csg_plus_values(Variable):
                value_type = float
                entity = FoyerFiscal
                label = "Assiette des plus-values soumis à la CSG"
                definition_period = YEAR

                def formula(foyer_fiscal, period):
                    return foyer_fiscal(
                        "plus_values_prelevement_forfaitaire_unique_ir", period
                    )

            class revenus_capital(Variable):
                value_type = float
                entity = Individu
                label = "Revenus du capital"
                definition_period = MONTH
                set_input = set_input_divide_by_period

                def formula(individu, period):
                    revenus_capitaux = (
                        max_(
                            0,
                            individu.foyer_fiscal(
                                "revenu_categoriel_capital", period.this_year
                            )
                            / 12,
                        )  # hypothèse car la variable est après certains abattements mais pour POTE 2022 on a sorti que ça pour le moment
                        + max_(
                            0,
                            individu.foyer_fiscal(
                                "revenus_capitaux_prelevement_liberatoire", period
                            ),
                        )
                        + max_(
                            0,
                            individu.foyer_fiscal(
                                "revenus_capitaux_prelevement_forfaitaire_unique_ir",
                                period,
                            ),
                        )
                    ) * individu.has_role(FoyerFiscal.DECLARANT_PRINCIPAL)

                    return revenus_capitaux

            class iaidrdi(Variable):
                value_type = float
                entity = FoyerFiscal
                label = "Impôt après imputation des réductions d'impôt"
                definition_period = YEAR

                def formula(foyer_fiscal, period, parameters):
                    """
                    Impôt après imputation des réductions d'impôt
                    """
                    ip_net = foyer_fiscal("ip_net", period)
                    reductions = foyer_fiscal("reductions", period)

                    return np.maximum(0, ip_net - reductions)

            class reduction_effective(Variable):
                value_type = float
                entity = FoyerFiscal
                label = "Impôt après imputation des réductions d'impôt"
                definition_period = YEAR

                def formula(foyer_fiscal, period):
                    """
                    Impôt après imputation des réductions d'impôt
                    """
                    ip_net = foyer_fiscal("ip_net", period)
                    reductions = foyer_fiscal("reductions", period)

                    return np.where(ip_net - reductions >= 0, reductions, ip_net)

            class rpns_autres_revenus(Variable):
                value_type = float
                entity = Individu
                label = "Autres revenus non salariés"
                definition_period = YEAR

                def formula(individu, period):
                    return np.maximum(individu("rpns_imposables", period), 0)

            class rfr_par_part(Variable):
                value_type = float
                entity = FoyerFiscal
                label = "Revenu fiscal de référence par part"
                definition_period = YEAR

                def formula(foyer_fiscal, period):
                    rfr = foyer_fiscal("rfr", period)
                    nbptr = foyer_fiscal("nbptr", period)
                    return rfr / nbptr

            ## adaptation du calcul de la prime de naissance de la paje aux données :
            ## normalement la prime de naissance est versée au 7e mois, mais comme on n'observe qu'une année civile de naissance on n'observe pas tous les 7e mois
            ## on modifie le calcul pour donner la prime le mois de la naissance
            ## ATTENTION comme on fait des simulations pour 2023 et après on ne modifie que la formule à partir de 2018 mais en fait il existe des formules passées
            class paje_naissance(Variable):
                calculate_output = calculate_output_add
                value_type = float
                entity = Famille
                label = "Allocation de naissance de la PAJE"
                reference = (
                    "http://vosdroits.service-public.fr/particuliers/F2550.xhtml"
                )
                definition_period = MONTH
                set_input = set_input_divide_by_period

                def formula_2018_04_01(famille, period, parameters):
                    """
                    Prestation d'accueil du jeune enfant - Allocation de naissance
                    Références législatives :git
                    https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073189&idArticle=LEGIARTI000006737121&dateTexte=&categorieLien=cid
                    """
                    af_nbenf = famille("af_nbenf", period)
                    base_ressources = famille(
                        "prestations_familiales_base_ressources", period
                    )
                    isole = not_(famille("en_couple", period))
                    biactivite = famille("biactivite", period)
                    paje = parameters(
                        period
                    ).prestations_sociales.prestations_familiales.petite_enfance.paje
                    bmaf = parameters(
                        period
                    ).prestations_sociales.prestations_familiales.bmaf.bmaf
                    prime_naissance = (
                        round(100 * paje.paje_cm2.montant.prime_naissance * bmaf) / 100
                    )

                    # # Versée au 7ème mois de grossesse
                    # diff_mois_naissance_periode_i = (famille.members('date_naissance', period).astype('datetime64[M]') - datetime64(period.start, 'M'))
                    # nb_enfants_eligibles = famille.sum(diff_mois_naissance_periode_i.astype('int') == 2, role = Famille.ENFANT)

                    # Versée le mois de la naissance
                    diff_mois_naissance_periode_i = famille.members(
                        "date_naissance", period
                    ).astype("datetime64[M]") - datetime64(period.start, "M")
                    nb_enfants_eligibles = famille.sum(
                        diff_mois_naissance_periode_i.astype("int") == 0,
                        role=Famille.ENFANT,
                    )

                    nbenf = (
                        af_nbenf + nb_enfants_eligibles
                    )  # Ajouter les enfants à naître

                    taux_plafond = (
                        (nbenf > 0)
                        + paje.paje_plaf.ne_adopte_apres_04_2018.majorations_enfants.premier_2eme_enfant
                        * min_(nbenf, 2)
                        + paje.paje_plaf.ne_adopte_apres_04_2018.majorations_enfants.troisieme_plus_enfant
                        * max_(nbenf - 2, 0)
                    )

                    majoration_isole_biactif = isole | biactivite

                    plafond_de_ressources = (
                        paje.paje_plaf.ne_adopte_apres_04_2018.taux_partiel.plafond_ressources_0_enfant
                        * taux_plafond
                        + (taux_plafond > 0)
                        * paje.paje_plaf.ne_adopte_apres_04_2018.taux_partiel.biactifs_parents_isoles
                        * majoration_isole_biactif
                    )

                    eligible_prime_naissance = base_ressources <= plafond_de_ressources

                    return (
                        prime_naissance
                        * eligible_prime_naissance
                        * nb_enfants_eligibles
                    )

            ## Creation de variable de déciles de revenu par part à l'échelle du ménage en attendant d'avoir le niveau de vie cad les minimas sociaux

            class revenus_menage(Variable):
                value_type = float
                entity = Menage
                label = "Somme des revenus nets des membres du ménage"
                definition_period = YEAR

                def formula(menage, period):
                    pensions_nettes_i = menage.members("pensions_nettes", period)
                    revenus_nets_du_capital_i = menage.members(
                        "revenus_nets_du_capital", period
                    )
                    revenus_nets_du_travail_i = menage.members(
                        "revenus_nets_du_travail", period
                    )
                    pensions_nettes = menage.sum(pensions_nettes_i)
                    revenus_nets_du_capital = menage.sum(revenus_nets_du_capital_i)
                    revenus_nets_du_travail = menage.sum(revenus_nets_du_travail_i)

                    return max_(
                        0,
                        pensions_nettes
                        + revenus_nets_du_capital
                        + revenus_nets_du_travail,
                    )

            class revenus_menage_par_uc(Variable):
                value_type = float
                entity = Menage
                label = "Somme des revenus nets des membres du ménage par unité de consommation"
                definition_period = YEAR

                def formula(menage, period):
                    revenus_menage = menage("revenus_menage", period)
                    uc = menage("unites_consommation", period)

                    return revenus_menage / uc

            class decile_revenus_menage_par_uc(Variable):
                value_type = Enum
                possible_values = Deciles
                default_value = Deciles.hors_champs
                entity = Menage
                label = "Décile de revenus du ménage divisés par le nombre d'unités de consommation"
                definition_period = YEAR

                def formula(menage, period):
                    revenus_menage_par_uc = menage("revenus_menage_par_uc", period)
                    weight_menages = menage("weight_menages", period)
                    labels = np.arange(1, 11)
                    method = 2
                    decile, values = mark_weighted_percentiles(
                        revenus_menage_par_uc,
                        labels,
                        weight_menages,
                        method,
                        return_quantiles=True,
                    )
                    return decile

            # Creation de variables de statistiques descriptives pour les graphiques de l'interface
            class menage_avec_enfants(Variable):
                value_type = bool
                entity = Menage
                label = "Menage avec des enfants à charge au sens des AF"
                definition_period = YEAR

                def formula(menage, period):
                    af_nbenf_i = menage.members.famille("af_nbenf", period.first_month)
                    af_nbenf = menage.sum(af_nbenf_i, role=Famille.DEMANDEUR)

                    return af_nbenf > 0

            # Création de variables de déciles pour les graphiques de l'interface

            class decile_rfr(Variable):
                value_type = Enum
                possible_values = Deciles
                default_value = Deciles.hors_champs
                entity = FoyerFiscal
                label = "Décile de revenu fiscal de référence"
                definition_period = YEAR

                def formula(foyer_fiscal, period):
                    rfr = foyer_fiscal("rfr", period)
                    weight_foyers = foyer_fiscal("weight_foyers", period)
                    labels = np.arange(1, 11)
                    method = 2
                    decile, values = mark_weighted_percentiles(
                        rfr, labels, weight_foyers, method, return_quantiles=True
                    )
                    # Alternative method
                    # decile, values = weighted_quantiles(rfr, labels, weight_foyers * menage_ordinaire_foyers_fiscaux, return_quantiles = True)
                    return decile

            class decile_rfr_par_part(Variable):
                value_type = Enum
                possible_values = Deciles
                default_value = Deciles.hors_champs
                entity = FoyerFiscal
                label = "Décile de revenu fiscal de référence par part fiscale"
                definition_period = YEAR

                def formula(foyer_fiscal, period):
                    rfr_par_part = foyer_fiscal("rfr_par_part", period)
                    weight_foyers = foyer_fiscal("weight_foyers", period)
                    labels = np.arange(1, 11)
                    method = 2
                    decile, values = mark_weighted_percentiles(
                        rfr_par_part,
                        labels,
                        weight_foyers,
                        method,
                        return_quantiles=True,
                    )
                    return decile

            class decile_niveau_de_vie(Variable):
                value_type = Enum
                possible_values = Deciles
                default_value = Deciles.hors_champs
                entity = Menage
                label = "Décile de revenus du ménage divisés par le nombre d'unités de consommation"
                definition_period = YEAR

                def formula(menage, period):
                    niveau_de_vie = menage("niveau_de_vie", period)
                    weight_menages = menage("weight_menages", period)
                    labels = np.arange(1, 11)
                    method = 2
                    decile, values = mark_weighted_percentiles(
                        niveau_de_vie,
                        labels,
                        weight_menages,
                        method,
                        return_quantiles=True,
                    )
                    return decile

            class allegement_general_mode_recouvrement(Variable):
                value_type = Enum
                possible_values = TypesAllegementModeRecouvrement
                default_value = TypesAllegementModeRecouvrement.fin_d_annee
                entity = Individu
                label = "Mode de recouvrement des allègements Fillon"
                definition_period = MONTH
                set_input = set_input_dispatch_by_period

            variables = [
                famille_id,
                foyer_fiscal_id,
                idfam,
                idfoy,
                idmen_original,
                idmen,
                menage_id,
                # noindiv,  Removed because creates a bug with dump/restore (it is an object and not an int or a float)
                quifam,
                quifoy,
                quimen,
                weight_familles,
                weight_foyers,
                weight_menages,
                wprm,
                weight_individus,
                reduction_effective,
                rfr_par_part,
                revenus_capital,
                iaidrdi,
                rfr_plus_values_hors_rni,
                assiette_csg_plus_values,
                rpns_imposables,
                rpns_autres_revenus,
                paje_naissance,
                revenus_menage,
                revenus_menage_par_uc,
                decile_revenus_menage_par_uc,
                menage_avec_enfants,
                decile_rfr,
                decile_rfr_par_part,
                decile_niveau_de_vie,
                allegement_general_mode_recouvrement,
                # plus_values_prelevement_forfaitaire_unique_ir,
            ]

            # Adaptation de variables du fait des variables de revenus du capital imputées

            for variable in variables:
                if variable == Variable:
                    continue
                try:
                    self.add_variable(variable)
                except VariableNameConflictError:
                    log.warning(
                        f"{variable.__name__} has been updated in leximpact-survey-scenario"
                    )
                    self.update_variable(variable)
            for neutralized_variable in neutralized_variables:
                log.info(f"Neutralizing {neutralized_variable}")
                if self.get_variable(neutralized_variable):
                    self.neutralize_variable(neutralized_variable)
            # for updated_variable in updated_variables:
            #    self.update_variable(updated_variable)

            # Création de variables de l'entité individu projetées sur le foyer fiscal nécéssaires
            # au calcul de stats ventilées par RFR
            for variable in projected_variables.get("foyer_fiscal", {}).get(
                "individu", []
            ):
                class_name = f"{variable}_foyer_fiscal"
                label = f"{variable} agrégée à l'échelle du foyer fiscal"

                def projection_formula_creator(variable):
                    def formula(foyer_fiscal, period, parameters):
                        result_i = foyer_fiscal.members(variable, period, options=[ADD])
                        result = foyer_fiscal.sum(result_i)
                        return result

                    formula.__name__ = "formula"

                    return formula

                variable_instance = type(
                    class_name,
                    (Variable,),
                    dict(
                        value_type=float,
                        entity=FoyerFiscal,
                        label=label,
                        definition_period=YEAR,
                        formula=projection_formula_creator(variable),
                    ),
                )

                self.add_variable(variable_instance)
                del variable_instance

            # Création de variables de l'entité individu projetées sur la famille pour les graphiques de l'UI
            for variable in projected_variables.get("famille", {}).get("individu", []):
                class_name = f"{variable}_famille"
                label = f"{variable} agrégée à l'échelle de la famille"

                def projection_formula_creator(variable):
                    def formula(famille, period, parameters):
                        result_i = famille.members(variable, period, options=[ADD])
                        result = famille.sum(result_i)
                        return result

                    formula.__name__ = "formula"

                    return formula

                variable_instance = type(
                    class_name,
                    (Variable,),
                    dict(
                        value_type=float,
                        entity=Famille,
                        label=label,
                        definition_period=YEAR,
                        formula=projection_formula_creator(variable),
                    ),
                )

                self.add_variable(variable_instance)
                del variable_instance

            ## Création de variables famille à l'échelle du ménage pour les graphiques sur l'UI
            for variable in projected_variables.get("menage", {}).get("famille", []):
                class_name = f"{variable}_menage"
                label = f"{variable} agrégée à l'échelle du menage"

                def projection_formula_creator(variable):
                    def formula(menage, period, parameters):
                        result_i = menage.members.famille(
                            variable, period, options=[ADD]
                        )
                        result = menage.sum(result_i, role=Famille.DEMANDEUR)
                        return result

                    formula.__name__ = "formula"

                    return formula

                variable_instance = type(
                    class_name,
                    (Variable,),
                    dict(
                        value_type=float,
                        entity=Menage,
                        label=label,
                        definition_period=YEAR,
                        formula=projection_formula_creator(variable),
                    ),
                )

                self.add_variable(variable_instance)
                del variable_instance

            for variable in projected_variables.get("menage", {}).get(
                "foyer_fiscal", []
            ):
                class_name = f"{variable}_menage"
                label = f"{variable} agrégée à l'échelle du menage"

                def projection_formula_creator(variable):
                    def formula(menage, period, parameters):
                        result_i = menage.members.foyer_fiscal(
                            variable, period, options=[ADD]
                        )
                        result = menage.sum(
                            result_i, role=FoyerFiscal.DECLARANT_PRINCIPAL
                        )
                        return result

                    formula.__name__ = "formula"

                    return formula

                variable_instance = type(
                    class_name,
                    (Variable,),
                    dict(
                        value_type=float,
                        entity=Menage,
                        label=label,
                        definition_period=YEAR,
                        formula=projection_formula_creator(variable),
                    ),
                )

                self.add_variable(variable_instance)
                del variable_instance

            for variable in projected_variables.get("menage", {}).get("individu", []):
                class_name = f"{variable}_menage"
                label = f"{variable} agrégée à l'échelle du menage"

                def projection_formula_creator(variable):
                    def formula(menage, period, parameters):
                        result_i = menage.members(variable, period, options=[ADD])
                        result = menage.sum(
                            result_i,
                        )
                        return result

                    formula.__name__ = "formula"

                    return formula

                variable_instance = type(
                    class_name,
                    (Variable,),
                    dict(
                        value_type=float,
                        entity=Menage,
                        label=label,
                        definition_period=YEAR,
                        formula=projection_formula_creator(variable),
                    ),
                )

                self.add_variable(variable_instance)
                del variable_instance

    leximpact_tax_benefits_system = leximpact_tbs_extension(
        openfisca_france_tax_benefit_system
    )

    return leximpact_tax_benefits_system
