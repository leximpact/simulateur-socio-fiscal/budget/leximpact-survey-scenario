from openfisca_core.reforms import Reform

from openfisca_france_data.model.base import (
    Variable,
)

variables_modifiees = [
    "irpp_economique",
    "csg_deductible_salaire",
    "csg_imposable_salaire",
    "csg_deductible_retraite",
    "csg_imposable_retraite",
    "af",
    "cf",
    "ars",
    "vieillesse_deplafonnee_salarie",
    "vieillesse_plafonnee_salarie",
    "vieillesse_deplafonnee_employeur",
    "vieillesse_plafonnee_employeur",
    "mmid_employeur",
    "allegement_cotisation_maladie",
    "chomage_employeur",
    "famille",
    "allegement_cotisation_allocations_familiales",
    "contribution_solidarite_autonomie",
    "fnal_contribution",
    "agirc_arrco_employeur",
    "contribution_equilibre_technique_employeur",
    "allegement_general",
]
# TODO dealer avec le fait que pour la csg le nom de la variable de ratio et à caler sont différentes


def create_tax_and_benefits_system_with_targets(
    ratios_calage: dict,
    tax_and_benefits_system=None,
    variables_a_caler: list = variables_modifiees,
    period_a_caler: int = 2024,
):
    for variable in variables_a_caler:
        if variable not in ratios_calage.keys():
            ratios_calage[variable] = 1

    class tax_benefits_system_cale(Reform):
        name = ""

        def apply(self):
            for variable in variables_a_caler:

                def projection_formula_creator(variable_name):
                    def formula(population, period, parameters):
                        return (
                            self.baseline.get_variable(variable_name).get_formula(
                                period_a_caler
                            )(population, period, parameters)
                            * ratios_calage[variable_name]
                        )

                    formula.__name__ = "formula"

                    return formula

                variable_instance = type(
                    variable,
                    (Variable,),
                    dict(
                        value_type=self.baseline.get_variable(variable).value_type,
                        entity=self.baseline.get_variable(variable).entity,
                        label=f"{self.baseline.get_variable(variable).label} après calage",
                        definition_period=self.baseline.get_variable(
                            variable
                        ).definition_period,
                        formula=projection_formula_creator(variable),
                    ),
                )

                self.update_variable(variable_instance)
                del variable_instance

    leximpact_tax_benefits_system_cale = tax_benefits_system_cale(
        tax_and_benefits_system
    )

    return leximpact_tax_benefits_system_cale
